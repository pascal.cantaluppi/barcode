# Barcode

## Read an scan barcodes with Flutter

<p>
<img src="https://gitlab.com/pascal.cantaluppi/barcode/-/raw/main/web/img/barcode.png" alt="Barcode" /></a>
</p>

This project is using the flutter_barcode_scanner plugin.

## Getting Started

```console
flutter run
```
